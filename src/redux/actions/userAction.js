import { message } from "antd";
import { userLocalStorage } from "../../services/localStorageService";
import { userService } from "../../services/userService";
import { SET_USER_INFOR } from "../reducer/constant/userConstant";

export const setLoginAction = (value) => {
  return {
    type: SET_USER_INFOR,
    payload: value,
  };
};
export const setLoginActionService = (userFormInput, onSuccess) => {
  return (dispatch) => {
    userService
      .postDangNhap(userFormInput)
      .then((res) => {
        //lưu vào local storage
        userLocalStorage.set(res.data.content);
        console.log(res);
        //đẩy lên redux store
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        onSuccess();
        //thông báo đăng nhập thành công
        message.success("Đăng nhập thành công!");
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra!");
      });
  };
};
