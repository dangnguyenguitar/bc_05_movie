import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { userLocalStorage } from "../../services/localStorageService";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userReducer.userInfor;
  });
  let handleLogout = () => {
    userLocalStorage.remove();
    window.location.href = "/"; //refresh lại trang và chuyển về homepage
  };
  const renderContent = () => {
    if (user) {
      return (
        <>
          <span>{user.hoTen}</span>
          <button onClick={handleLogout} className="border-2 rounded px-5 py-2">
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className="border-2 rounded px-5 py-2">Đăng nhập</button>
          </NavLink>
          <button className="border-2 rounded px-5 py-2">Đăng Ký</button>
        </>
      );
    }
  };
  return <div className="space-x-5">{renderContent()}</div>;
}
