import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userService } from "../../services/userService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFOR } from "../../redux/reducer/constant/userConstant";
import { userLocalStorage } from "../../services/localStorageService";
import Lottie from "lottie-react";
import bg_animate from "../../assets/88454-christmas.json";
import { setLoginActionService } from "../../redux/actions/userAction";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userService
      .postDangNhap(values)
      .then((res) => {
        console.log(res);
        dispatch({
          type: SET_USER_INFOR,
          payload: res.data.content,
        });
        message.success("Đăng nhập thành công!");
        //lưu vào local storage
        userLocalStorage.set(res.data.content);
        setTimeout(() => {
          // window.location.href = "/";
          navigate("/"); //dùng navigate để tránh việc windows load trang khi trở về homepage
        }, 1000);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đã có lỗi xảy ra!");
      });
  };
  const onFinishReduxThunk = (userFormInput) => {
    let onNavigate = () => {
      setTimeout(() => {
        navigate("/");
      }, 1000);
    };
    dispatch(setLoginActionService(userFormInput, onNavigate));
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <div className="container p-5 flex">
        <div className="h-full w-1/2">
          <Lottie animationData={bg_animate} loop={true} />;
        </div>
        <div className="h-full w-1/2 mt-60">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            className="text-center"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinishReduxThunk}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              name="remember"
              valuePropName="checked"
              wrapperCol={{
                offset: 8,
                span: 6,
              }}
            >
              <Checkbox>Remember me</Checkbox>
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 6,
              }}
            >
              <Button
                className="bg-red-800 text-white hover:bg-white"
                htmlType="submit"
              >
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
