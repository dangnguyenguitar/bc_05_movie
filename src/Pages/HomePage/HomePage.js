import React, { useEffect, useState } from "react";
import Header from "../../Components/Header/Header";
import { movieService } from "../../services/movieService";
import MovieList from "./MovieList/MovieList";
import MovieTabs from "./MovieTabs/MovieTabs";

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService
      .getDanhSachPhim()
      .then((res) => {
        console.log(res);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div>
      <div className="container mx-auto">
        {/* <Header /> */}
        <MovieList movieArr={movieArr} />
        <MovieTabs />
      </div>
    </div>
  );
}
