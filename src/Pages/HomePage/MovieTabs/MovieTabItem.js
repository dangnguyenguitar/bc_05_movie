import moment from "moment/moment"; // format lại date dành cho front end. Bởi vì backend sẽ chỉ trả một cục date không có định dạng sẵn
import React from "react";

export default function MovieTabItem({ movie }) {
  return (
    <div className="flex p-3">
      <img
        className="w-24 h-40 object-cover mr-5 rounded"
        src={movie.hinhAnh}
        alt=""
      />
      <div>
        <h5 className="font-medium mb-5">{movie.tenPhim}</h5>
        <div className="grid grid-cols-3 gap-4">
          {movie.lstLichChieuTheoPhim.slice(0, 9).map((item) => {
            return (
              <span className="bg-red-500 text-white rounded p-2">
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
