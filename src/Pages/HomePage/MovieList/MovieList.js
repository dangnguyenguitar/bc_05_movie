import React from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function MovieList({ movieArr }) {
  const renderMovieList = () => {
    return movieArr.slice(0, 15).map((item) => {
      let { maPhim, tenPhim, hinhAnh, moTa } = item;
      return (
        <Card
          hoverable
          style={{
            width: "100%",
          }}
          cover={<img src={hinhAnh} className="h-80 object-cover" />}
        >
          {/* <Meta
            title={tenPhim}
            description={moTa.length < 60 ? moTa : moTa.slice(0, 60) + "..."}
          /> */}
          <NavLink
            to={`detail/${maPhim}`}
            className={
              "bg-cyan-500 px-5 py-2 rounded text-white shadow hover:shadow-xl transition hover:text-black"
            }
          >
            Xem chi tiết
          </NavLink>
        </Card>
      );
    });
  };
  return <div className="grid grid-cols-5 gap-5">{renderMovieList()}</div>;
}
